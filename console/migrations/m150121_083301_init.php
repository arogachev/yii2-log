<?php

use arogachev\log\components\Migration;
use yii\db\Schema;

class m150121_083301_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Log models

        $this->createTable('log_models', [
            'id' => Schema::TYPE_PK,
            'class' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        // Log attributes

        $this->createTable('log_attributes', [
            'id' => Schema::TYPE_PK,
            'log_model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->addForeignKey(
            null,
            'log_attributes',
            'log_model_id',
            'log_models',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Log models changes

        $this->createTable('log_models_changes', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_SMALLINT . '(1) NOT NULL',
            'log_model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'url' => Schema::TYPE_STRING,
            'user_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey(
            null,
            'log_models_changes',
            'log_model_id',
            'log_models',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Log attributes changes

        $this->createTable('log_attributes_changes', [
            'id' => Schema::TYPE_PK,
            'log_model_change_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'log_attribute_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'new_value' => Schema::TYPE_TEXT,
            'old_value' => Schema::TYPE_TEXT,
            'is_many_to_many' => Schema::TYPE_BOOLEAN . ' NOT NULL',
        ]);

        $this->addForeignKey(
            null,
            'log_attributes_changes',
            'log_model_change_id',
            'log_models_changes',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            null,
            'log_attributes_changes',
            'log_attribute_id',
            'log_attributes',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Log many-to-many changes

        $this->createTable('log_many_to_many_changes', [
            'id' => Schema::TYPE_PK,
            'log_attribute_change_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type' => Schema::TYPE_INTEGER . ' NOT NULL',
            'value' => Schema::TYPE_TEXT . ' NOT NULL',
        ]);

        $this->addForeignKey(
            null,
            'log_many_to_many_changes',
            'log_attribute_change_id',
            'log_attributes_changes',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKeyByLink('log_attributes', 'log_models', 'id');
        $this->dropForeignKeyByLink('log_models_changes', 'log_models', 'id');
        $this->dropForeignKeyByLink('log_attributes_changes', 'log_models_changes', 'id');
        $this->dropForeignKeyByLink('log_attributes_changes', 'log_attributes', 'id');
        $this->dropForeignKeyByLink('log_many_to_many_changes', 'log_attributes_changes', 'id');

        $this->dropTable('log_models');
        $this->dropTable('log_attributes');
        $this->dropTable('log_models_changes');
        $this->dropTable('log_attributes_changes');
        $this->dropTable('log_many_to_many_changes');
    }
}
