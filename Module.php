<?php

namespace arogachev\log;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'arogachev\log\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setViewPath('@vendor/arogachev/yii2-log/views');
    }
}
