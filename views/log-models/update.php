<?php

/* @var $this yii\web\View */
/* @var $model arogachev\log\models\LogModel */

$this->title = 'Update log model: ' . $model->class;
$this->params['breadcrumbs'][] = ['label' => 'Log models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->class, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="log-model-update">
    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', ['model' => $model]) ?>
</div>
