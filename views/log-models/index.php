<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel arogachev\log\models\search\LogModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log models';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="log-model-index">
    <h1><?= $this->title ?></h1>

    <p>
        <?= Html::a('Create log model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'class',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]) ?>
</div>
