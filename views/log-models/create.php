<?php

/* @var $this yii\web\View */
/* @var $model arogachev\log\models\LogModel */

$this->title = 'Create log model';
$this->params['breadcrumbs'][] = ['label' => 'Log models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="log-model-create">
    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', ['model' => $model]) ?>
</div>
