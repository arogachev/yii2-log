<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model arogachev\log\models\LogModel */

$this->title = $model->class;
$this->params['breadcrumbs'][] = ['label' => 'Log models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="log-model-view">
    <h1><?= $this->title ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'class',
        ],
    ]) ?>
</div>
