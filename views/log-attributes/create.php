<?php

/* @var $this yii\web\View */
/* @var $model arogachev\log\models\LogAttribute */

$this->title = 'Create log attribute';
$this->params['breadcrumbs'][] = ['label' => 'Log attributes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="log-attribute-create">
    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', ['model' => $model]) ?>
</div>
