<?php

use arogachev\log\models\LogModel;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel arogachev\log\models\search\LogAttributeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log attributes';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="log-attribute-index">
    <h1><?= $this->title ?></h1>

    <p>
        <?= Html::a('Create log attribute', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'log_model_id',
                'filter' => LogModel::getList(),
                'value' => function ($model) {
                    /* @var $model arogachev\log\models\LogAttribute */

                    return $model->logModel->class;
                },
            ],
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]) ?>
</div>
