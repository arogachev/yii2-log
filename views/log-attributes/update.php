<?php

/* @var $this yii\web\View */
/* @var $model arogachev\log\models\LogAttribute */

$this->title = 'Update log attribute: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Log attributes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="log-attribute-update">
    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', ['model' => $model]) ?>
</div>
