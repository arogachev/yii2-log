<?php

use arogachev\log\models\LogModel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model arogachev\log\models\LogAttribute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-attribute-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'log_model_id')->dropDownList(LogModel::getList(), ['prompt' => 'Nothing selected']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
