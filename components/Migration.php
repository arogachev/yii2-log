<?php

namespace arogachev\log\components;

use yii\db\Migration as BaseMigration;

class Migration extends BaseMigration
{
    /**
     * @inheritdoc
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        $customName = ($name !== null) ? $name : static::getPrimaryKeyName($table, $columns);

        parent::addPrimaryKey($customName, $table, $columns);
    }

    /**
     * @inheritdoc
     */
    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        $customName = ($name !== null) ? $name : static::getForeignKeyName($table, $refTable, $refColumns);

        parent::addForeignKey($customName, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * @inheritdoc
     */
    public function dropForeignKeyByLink($table, $refTable, $refColumns)
    {
        $foreignKeyName = static::getForeignKeyName($table, $refTable, $refColumns);

        parent::dropForeignKey($foreignKeyName, $table);
    }

    /**
     * @param string $table
     * @param mixed $columns
     * @return string
     */
    protected static function getPrimaryKeyName($table, $columns)
    {
        $columnsString = static::getColumnsString($columns);

        return "pk_{$table}_{$columnsString}";
    }

    /**
     * @param string $table
     * @param string $refTable
     * @param mixed $refColumns
     * @return string
     */
    protected static function getForeignKeyName($table, $refTable, $refColumns)
    {
        $refColumnsString = static::getColumnsString($refColumns);

        return "fk_{$table}_{$refTable}_{$refColumnsString}";
    }

    /**
     * @param mixed $columns
     * @return string
     */
    protected static function getColumnsString($columns)
    {
        if (!is_array($columns)) {
            return $columns;
        } else {
            $columnsArray = preg_split('/\s*,\s*/', $columns, -1, PREG_SPLIT_NO_EMPTY);

            return implode('_', $columnsArray);
        }
    }
}
