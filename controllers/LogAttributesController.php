<?php

namespace arogachev\log\controllers;

use arogachev\log\models\LogAttribute;
use arogachev\log\models\search\LogAttributeSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class LogAttributesController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LogAttributeSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', ['model' => $this->findModel($id)]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogAttribute;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ['model' => $model]);
        }
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * @param integer $id
     * @return LogAttribute
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = LogAttribute::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('This log attribute is not exist.');
        }
    }
}
