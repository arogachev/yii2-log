<?php

namespace arogachev\log\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $log_model_id
 * @property string $name
 *
 * @property LogModel $logModel
 */
class LogAttribute extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_model_id', 'name'], 'required'],
            ['log_model_id', 'exist', 'targetClass' => LogModel::className(), 'targetAttribute' => 'id'],
            ['name', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_model_id' => 'Log Model ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogModel()
    {
        return $this->hasOne(LogModel::className(), ['id' => 'log_model_id']);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        /* @var $model \yii\db\ActiveRecord */
        $model = new $this->logModel->class;

        return $model->getAttributeLabel($this->name);
    }

    /**
     * @return array
     */
    public static function getList()
    {
        /* @var $models LogAttribute[] */
        $models = static::find()->with('logModel')->all();
        $result = [];

        foreach ($models as $model) {
            $result[$model->id] = $model->getLabel();
        }

        asort($result);

        return $result;
    }
}
