<?php

namespace arogachev\log\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property string $class
 *
 * @property LogAttribute[] $logAttributes
 * @property LogModelChange[] $logModelChanges
 */
class LogModel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_models';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['class', 'required'],
            ['class', 'string', 'max' => 255],
            ['class', 'validateClass'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateClass($attribute)
    {
        if (!class_exists($this->$attribute)) {
            $label = $this->getAttributeLabel($attribute);
            $this->addError($attribute, "$label «{$this->$attribute}» does not exist.");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class' => 'Class',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAttributes()
    {
        return $this->hasMany(LogAttribute::className(), ['log_model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogModelChanges()
    {
        return $this->hasMany(LogModelChange::className(), ['log_model_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $items = static::find()->orderBy(['class' => SORT_ASC])->asArray()->all();

        return ArrayHelper::map($items, 'id', 'class');
    }
}
