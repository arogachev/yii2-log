<?php

namespace arogachev\log\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property integer $id
 * @property integer $type
 * @property integer $log_model_id
 * @property integer $model_id
 * @property string $url
 * @property integer $user_id
 * @property string $created_at
 *
 * @property LogAttributeChange[] $logAttributesChanges
 * @property LogAttribute[] $logAttributes
 * @property LogModel $logModel
 */
class LogModelChange extends ActiveRecord
{
    /**
     * Type - Create
     */
    const TYPE_CREATE = 1;

    /**
     * Type - Update
     */
    const TYPE_UPDATE = 2;

    /**
     * Type - Delete
     */
    const TYPE_DELETE = 3;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_models_changes';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'log_model_id' => 'Log Model ID',
            'model_id' => 'Model ID',
            'url' => 'Url',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->url = Yii::$app->request ? Yii::$app->request->url : null;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAttributesChanges()
    {
        return $this->hasMany(LogAttributeChange::className(), ['log_model_change_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAttributes()
    {
        return $this->hasMany(LogAttributeChange::className(), ['log_attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogModel()
    {
        return $this->hasOne(LogModel::className(), ['id' => 'log_model_id']);
    }
}
