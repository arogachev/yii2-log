<?php

namespace arogachev\log\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $log_model_change_id
 * @property integer $log_attribute_id
 * @property string $new_value
 * @property string $old_value
 * @property integer $is_many_to_many
 *
 * @property LogModelChange $logModelChange
 * @property LogAttribute $logAttribute
 * @property LogManyToManyChange $logManyToManyChanges
 */
class LogAttributeChange extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_attributes_changes';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_model_change_id' => 'Log Model Change ID',
            'log_attribute_id' => 'Attribute ID',
            'new_value' => 'New Value',
            'old_value' => 'Old Value',
            'is_many_to_many' => 'Is Many-to-many'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogModelChange()
    {
        return $this->hasOne(LogModelChange::className(), ['id' => 'log_model_change_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAttribute()
    {
        return $this->hasOne(LogAttribute::className(), ['id' => 'log_attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogManyToManyChanges()
    {
        return $this->hasMany(LogManyToManyChange::className(), ['log_attribute_change_id' => 'id']);
    }
}
