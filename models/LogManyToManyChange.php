<?php

namespace arogachev\log\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $log_attribute_change_id
 * @property integer $type
 * @property string $value
 *
 * @property LogAttributeChange $logAttributeChange
 */
class LogManyToManyChange extends ActiveRecord
{
    /**
     * Type - Before
     */
    const TYPE_BEFORE = 1;

    /**
     * Type - After
     */
    const TYPE_AFTER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_many_to_many_changes';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_attribute_change_id' => 'Log Attribute Change ID',
            'type' => 'Type',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAttributeChange()
    {
        return $this->hasOne(LogAttributeChange::className(), ['id' => 'log_attribute_change_id']);
    }
}
