<?php

namespace arogachev\log\behaviors;

use arogachev\log\models\LogAttributeChange;
use arogachev\log\models\LogManyToManyChange;
use arogachev\log\models\LogModel;
use arogachev\log\models\LogModelChange;
use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;

class LogBehavior extends Behavior
{
    /**
     * @var \arogachev\log\models\LogModel
     */
    protected $_logModel;

    /**
     * @var \yii\db\ActiveRecord
     */
    protected $_oldModel;

    /**
     * @var array
     */
    public $valuesProcessing = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];
    }

    /**
     * @throws Exception
     */
    public function afterFind()
    {
        /* @var $model \yii\db\ActiveRecord */
        $model = $this->owner;

        $this->initLogModel();

        $this->_oldModel = clone $model;
    }

    public function afterUpdate()
    {
        /* @var $model \yii\db\ActiveRecord  */
        $model = $this->owner;

        $isModelChangeCreated = false;

        foreach ($this->_logModel->logAttributes as $attribute) {
            $oldValue = $this->_oldModel->{$attribute->name};
            $newValue = $model->{$attribute->name};

            if (is_array($oldValue)) {
                sort($oldValue);
                sort($newValue);
            }

            if ($oldValue != $newValue) {
                if (!$isModelChangeCreated) {
                    $logModelChange = new LogModelChange;
                    $logModelChange->type = LogModelChange::TYPE_UPDATE;
                    $logModelChange->log_model_id = $this->_logModel->id;
                    $logModelChange->model_id = $model->primaryKey;
                    $logModelChange->save(false);

                    $isModelChangeCreated = true;
                }

                if (isset($this->valuesProcessing[$attribute->name])) {
                    $oldValue = call_user_func($this->valuesProcessing[$attribute->name], $this->_oldModel);
                    $newValue = call_user_func($this->valuesProcessing[$attribute->name], $model);
                }

                /* @var $logModelChange LogModelChange */

                $logAttributeChange = new LogAttributeChange;
                $logAttributeChange->log_model_change_id = $logModelChange->id;
                $logAttributeChange->log_attribute_id = $attribute->id;

                if (!is_array($oldValue)) {
                    $logAttributeChange->old_value = $oldValue;
                    $logAttributeChange->new_value = $newValue;
                    $logAttributeChange->is_many_to_many = 0;
                } else {
                    $logAttributeChange->is_many_to_many = 1;
                }

                $logAttributeChange->save(false);

                if (is_array($oldValue)) {
                    foreach ($oldValue as $singleValue) {
                        $logManyToManyChange = new LogManyToManyChange;
                        $logManyToManyChange->log_attribute_change_id = $logAttributeChange->id;
                        $logManyToManyChange->type = LogManyToManyChange::TYPE_BEFORE;
                        $logManyToManyChange->value = $singleValue;
                        $logManyToManyChange->save(false);
                    }

                    foreach ($newValue as $singleValue) {
                        $logManyToManyChange = new LogManyToManyChange;
                        $logManyToManyChange->log_attribute_change_id = $logAttributeChange->id;
                        $logManyToManyChange->type = LogManyToManyChange::TYPE_AFTER;
                        $logManyToManyChange->value = $singleValue;
                        $logManyToManyChange->save(false);
                    }
                }
            }
        }
    }


    /**
     * @throws Exception
     */
    protected function initLogModel()
    {
        $className = $this->owner->className();

        if (($model = LogModel::find(['class' => $className])->one()) === null) {
            throw new Exception("Model '$className' is not in the list of log models.");
        }

        if (!$model->logAttributes) {
            throw new Exception("No log attributes specified for model '$className'.");
        }

        $this->_logModel = $model;
    }
}
